export const URL_PATH = {
  HOME: "/",
  TERMS: "/terms",
  POLICY: "/policy",
  USER: "/user",
};

export const OUTSIDE_URL = {
  LINE: { FOLLOW: " https://lin.ee/Q28r1Nv" },
  GOOGLE: {
    API_POLICY:
      "https://developers.google.com/terms/api-services-user-data-policy",
  },
};
