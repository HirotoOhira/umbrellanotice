export type LineUserState = {
  related: boolean;
  serialNumber: string;
};
