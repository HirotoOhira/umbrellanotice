export type WeatherState = {
  location: string;
  noticeTime: string;
  silentNotice: boolean;
};
