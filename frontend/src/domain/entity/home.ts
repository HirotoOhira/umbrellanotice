export type HomeState = {
  isHomePage: boolean;
  topViewHeight: number;
  scrollTop: number;
};
