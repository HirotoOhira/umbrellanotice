export type AmplifyError = {
  code: string; // e.g. "InvalidParameterException"
  message: string; // e.g. "Invalid email address format."
  name: string; // e.g. "InvalidParameterException"
};
